<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="8V9qw8DmCMO7b6bYiOktZbQThYlw0UGm7abQ7ZAG">
    <meta name="author" content="Agung Kurniawan">
    <meta name="robots" content="">
    <meta name="keywords" content="dashboard, wallet, topup, transaction">
    <meta name="description" content="Yukk Tes Fullstack Developer">
    <meta property="og:title" content="Dompet">
    <meta property="og:description" content="DOMPET | Login">
    <meta name="format-detection" content="telephone=no">

    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('../templates/images/favicon.png') }}">

    <!-- Page Title Here -->
    <title>@yield('title')</title>

    <link href="{{ URL::asset('../templates/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('../templates/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('../templates/vendor/toastr/css/toastr.min.css') }}" rel="stylesheet" type="text/css"/>
    @stack('styles')
</head>

<body body class="vh-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-lg-6 col-md-12 col-sm-12 mx-auto align-self-center">
                    @yield('content')
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="pages-left h-100">
                        <div class="login-content">
                            <a href="index.html"><img src="{{ URL::asset('../templates/images/logo-full.png') }}" class="mb-3" alt=""></a>
                        </div>
                        <div class="login-media text-center">
                            <img src="{{ URL::asset('../templates/images/login.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::asset('../templates/vendor/global/global.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/vendor/jquery-nice-select/js/jquery.nice-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/dlabnav-init.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/demo.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/vendor/toastr/js/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/plugins-init/toastr-init.js') }}" type="text/javascript"></script>
  @stack('scripts')
</body>

</html>
