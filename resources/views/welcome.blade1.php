<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="8V9qw8DmCMO7b6bYiOktZbQThYlw0UGm7abQ7ZAG">
    <meta name="author" content="DexignLab">
    <meta name="robots" content="">
    <meta name="keywords"
        content="bootstrap admin, card, clean, credit card, dashboard template, elegant, invoice, modern, money, transaction, Transfer money, user interface, wallet">
    <meta name="description" content="Some description for the page">
    <meta property="og:title" content="Dompet - Payment Admin Dashboard Bootstrap Template">
    <meta property="og:description" content="Laravel | Page Login">
    <meta property="og:image" content="../social-image.png">
    <meta name="format-detection" content="telephone=no">

    <!-- Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- favicon -->
    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('../templates/images/favicon.png') }}">

    <!-- Page Title Here -->
    <title>DOMPET | Login</title>

    <link href="{{ URL::asset('../templates/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}"
        rel="stylesheet">
    <link href="{{ URL::asset('../templates/css/style.css') }}" rel="stylesheet">
</head>

<body body class="vh-100">
    <div class="authincation h-100">
        <div class="container-fluid h-100">
            <div class="row h-100">
                <div class="col-lg-6 col-md-12 col-sm-12 mx-auto align-self-center">
                    <div class="login-form">
                        <div class="text-center">
                            <h3 class="title">Login</h3>
                            <p>Login ke akun Anda untuk mulai menggunakan DOMPET</p>
                        </div>
                        <form action="#">
                            <div class="mb-4">
                                <label class="mb-1 text-dark">Email</label>
                                <input type="email" class="form-control form-control" value="hello@example.com">
                            </div>
                            <div class="mb-4 position-relative">
                                <label class="mb-1 text-dark">Password</label>
                                <input type="password" id="dlab-password" class="form-control form-control"
                                    value="Password">
                                <span class="show-pass eye">
                                    <i class="fa fa-eye-slash"></i>
                                    <i class="fa fa-eye"></i>
                                </span>
                            </div>
                            <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                <div class="mb-4">
                                    <div class="form-check custom-checkbox mb-3">
                                        <input type="checkbox" class="form-check-input" id="customCheckBox1"
                                            required="">
                                        <label class="form-check-label mt-1" for="customCheckBox1">Remember my
                                            preference</label>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <a href="page-forgot-password.html" class="btn-link text-primary">Forgot
                                        Password?</a>
                                </div>
                            </div>
                            <div class="text-center mb-4">
                                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                            </div>
                            <h6 class="login-title"><span>Or continue with</span></h6>

                            <div class="mb-3">
                                <ul class="d-flex align-self-center justify-content-center">
                                    <li><a target="_blank" href="https://www.facebook.com/"
                                            class="fab fa-facebook-f btn-facebook"></a></li>
                                    <li><a target="_blank" href="https://www.google.com/"
                                            class="fab fa-google-plus-g btn-google-plus mx-2"></a></li>
                                    <li><a target="_blank" href="https://www.linkedin.com/"
                                            class="fab fa-linkedin-in btn-linkedin me-2"></a></li>
                                    <li><a target="_blank" href="https://twitter.com/"
                                            class="fab fa-twitter btn-twitter"></a></li>
                                </ul>
                            </div>
                            <p class="text-center">Not registered?
                                <a class="btn-link text-primary" href="page-register.html">Register</a>
                            </p>
                        </form>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6">
                    <div class="pages-left h-100">
                        <div class="login-content">
                            <a href="index.html"><img src="{{ URL::asset('../templates/images/logo-full.png') }}"
                                    class="mb-3" alt=""></a>
                        </div>
                        <div class="login-media text-center">
                            <img src="{{ URL::asset('../templates/images/login.png') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ URL::asset('../templates/vendor/global/global.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/vendor/jquery-nice-select/js/jquery.nice-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/custom.min.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/dlabnav-init.js') }}" type="text/javascript"></script>
    <script src="{{ URL::asset('../templates/js/demo.js') }}" type="text/javascript"></script>

</body>

</html>
