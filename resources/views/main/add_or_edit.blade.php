@extends('layouts.admin')
@section('title')
  DOMPET | Transaction
@endsection
@section('header')
  {{$title}}
@endsection
@section('content')
<div class="col-lg-12">
  <div class="card">
    <div class="card-body">
      <div class="form-validation">
        <form class="needs-validation" novalidate action="{{route('transaction')}}" method="post">
          @csrf
          <div class="row">
            <div class="col-xl-6">
              <div class="mb-3 row">
                <label class="col-lg-4 col-form-label" for="typeid">Tipe Transaksi <span class="text-danger">*</span>
                </label>
                <div class="col-lg-6">
                  <select class="default-select wide form-control" id="typeid" name="typeid" required value="{{old('typeid')}}">
                    <option data-display="Select">Please select</option>
                    @foreach($type as $key => $value)
                    <option value="{{$value->typeid}}">{{$value->type}}</option>
                    @endforeach
                  </select>
                  <div class="invalid-feedback"> Please select a type. </div>
                </div>
              </div>
              <div class="mb-3 row">
                <label class="col-lg-4 col-form-label" for="amount">Amount <span class="text-danger">*</span>
                </label>
                <div class="col-lg-6">
                  <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="{{old('amount')}}" required>
                  <div class="invalid-feedback"> Please enter an Amount. </div>
                </div>
              </div>
              <div class="mb-3 row">
                <label class="col-lg-4 col-form-label" for="desc">Description <span class="text-danger">*</span>
                </label>
                <div class="col-lg-6">
                  <textarea class="form-control" id="desc" name="desc" rows="5" placeholder="Description"  required>{{old('desc')}}</textarea>
                  <div class="invalid-feedback"> Please enter a description. </div>
                </div>
              </div>
              <div class="mb-3 row">
                <div class="col-lg-8 ms-auto">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection


@push('scripts')
<script>
    (function () {
        'use strict'
        console.log("{{$errors}}");
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
            }

            form.classList.add('was-validated')
            }, false)
        })

        @if(count($errors) > 0)
            toastr.error("{{$errors->first('message')}}", "Error!", {
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })
        @endif

        @if(session('success'))
            toastr.success("{{session('success')}}", "Sukses!", {
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })
        @endif
        $('#amount').on('input', function() {
            // Get the value from the input field
            var inputValue = $(this).val();
            
            // Use a regular expression to check if the input contains only numbers
            var numericRegex = /^[0-9]*$/;
            
            if (inputValue.match(numericRegex)) {
                // Input is numeric, clear validation message
            } else {
                $(this).val('');
                // Input is not numeric, show validation message
                toastr.error("Please enter a valid numeric value.", "Error!", {
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })
            }
        });
    })()

  
</script>
@endpush