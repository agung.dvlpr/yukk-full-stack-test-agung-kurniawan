@extends('layouts.admin')
@section('title')
  DOMPET | Dashboard
@endsection
@section('header')
  Riwayat Saldo
@endsection
@section('content')
<div class="row">
  <div class="col-xl-12 col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Filter</h4>
        </div>
        <div class="card-body">
          <div class="basic-form">
            <form action="{{ route('transaction') }}" method="GET">
              <div class="row">
                  <div class="mb-3 col-md-6">
                      <label class="form-label">Tipe</label>
                      <select class="default-select wide form-control" id="type" name="type" required>
                        <option value="0">Semua</option>
                        @foreach($data_type as $key => $value)
                        <option value="{{$value->typeid}}" @if($type == $value->typeid) selected @endif>{{$value->type}}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="mb-3 col-md-6">
                      <label class="form-label">Search</label>
                      <input type="text" class="form-control" name="search" placeholder="Search" value="{{$search}}">
                  </div>
                  <div class="mb-3 col-md-6">
                      <label class="form-label">Periode</label>
                      <input type="text" class="form-control input-daterange-datepicker" name="daterange" placeholder="Periode" value="{{$daterange}}">
                  </div>
                  <div class="mb-3 col-md-6">
                      <button type="submit" class="btn btn-primary btn-sm">Search</button>
                  </div>
              </div>
            </form>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xl-12 tab-content">
    <div class="tab-pane fade show active" id="AllTransaction" role="tabpanel" aria-labelledby="transaction-tab">
      <div class="table-responsive fs-14">
        <table id="wallet-datatable" class="table card-table display mb-4 dataTablesCard text-black">
          <thead>
            <tr>
              <th>Kode</th>
              <th>Date</th>
              <th>Type</th>
              <th>Debit</th>
              <th>Credit</th>
              <th>Balance</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            @foreach($data as $key => $value)
            <tr>
              <td>
                <span>{{$value->walletcode}}</span>
              </td>
              <td>
                <span>{{$value->created_at}}</span>
              </td>
              <td>
               <span>{{$value->type->type}}</span>
              </td>
              <td>
                <span>{{number_format($value->debit, 2, ',', '.')}}</span>
              </td>
               <td>
                <span>{{number_format($value->credit, 2, ',', '.')}}</span>
              </td>
               <td>
                <span>{{number_format($value->balance, 2, ',', '.')}}</span>
              </td>
               <td>
                <span>{{$value->desc}}</span>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        <div class="d-flex justify-content-center">
          {!! $data->onEachSide(1)->appends(['search' => request('search'), 'daterange' => request('daterange'), 'type' => request('type')])->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script>
$(function() {
  $('.input-daterange-datepicker').daterangepicker({
    autoUpdateInput: false,
    locale: {
      format: 'YYYY-MM-DD',
      cancelLabel: 'Clear'
    },
    opens: 'left'
  }, function(start, end, label) {
    startDate = start;
    endDate = end;
    console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
  });

  $('.input-daterange-datepicker').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(`${picker.startDate.format('YYYY-MM-DD')} - ${picker.endDate.format('YYYY-MM-DD')}`);
  });

  $('.input-daterange-datepicker').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });
});
</script>
@endpush