@extends('layouts.auth')
@section('title')
  DOMPET | Login
@endsection
@section('content')
<div class="login-form">
    <div class="text-center">
        <h3 class="title">Login</h3>
        <p>Login ke akun Anda untuk mulai menggunakan DOMPET</p>
    </div>
    <form action="{{ route('authenticate') }}" method="post" class="needs-validation" novalidate>
        @csrf
        <div class="mb-4">
            <label class="mb-1 text-dark">Email</label>
            <input type="email" class="form-control" placeholder="Email" id="email" name="email" value="{{ old('email') }}" required>
            @if ($errors->has('email'))
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
            @endif
        </div>
        <div class="mb-4 position-relative">
            <label class="mb-1 text-dark">Password</label>
            <input type="password" class="form-control" placeholder="Password" id="password" name="password" required>
            <span class="show-pass eye">
                <i class="fa fa-eye-slash"></i>
                <i class="fa fa-eye"></i>
            </span>
            @if ($errors->has('password'))
                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
            @endif
        </div>
        <div class="text-center mb-4">
            <button type="submit" class="btn btn-primary btn-block">Login</button>
        </div>
        <p class="text-center">Not registered?
            <a class="btn-link text-primary" href="{{route('register')}}">Register</a>
        </p>
    </form>
</div>
@endsection

@push('scripts')
<script>
    (function () {
        'use strict'

        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')

        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
            }

            form.classList.add('was-validated')
            }, false)
        })
        
        @if(count($errors) > 0)
            toastr.error("{{$errors->first('email')}}", "Error!", {
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })
        @endif

        @if(session('success'))
            toastr.success("{{session('success')}}", "Success!", {
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    closeButton: !0,
                    debug: !1,
                    newestOnTop: !0,
                    progressBar: !0,
                    preventDuplicates: !0,
                    onclick: null,
                    showDuration: "300",
                    hideDuration: "1000",
                    extendedTimeOut: "1000",
                    showEasing: "swing",
                    hideEasing: "linear",
                    showMethod: "fadeIn",
                    hideMethod: "fadeOut",
                    tapToDismiss: !1
                })
        @endif

        jQuery(".show-pass").on("click", (function () {
          jQuery(this).toggleClass("active"), "password" == jQuery(".password").attr("type") ? jQuery(".password").attr("type", "text") : "text" == jQuery(".password").attr("type") && jQuery(".password").attr("type", "password")
        }))
    })()

</script>
@endpush
