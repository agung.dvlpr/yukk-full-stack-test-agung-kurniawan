@extends('layouts.auth')
@section('title')
  DOMPET | Register
@endsection
@section('content')
<div class="login-form">
    <div class="text-center">
        <h3 class="title">Register</h3>
        <p>Register ke akun Anda untuk mulai menggunakan DOMPET</p>
    </div>
    <form action="{{ route('register') }}" method="post" class="needs-validation" novalidate>
        @csrf
        <div class="mb-4">
            <label class="mb-1 text-dark">Name</label>
            <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{ old('name') }}" required>
            @if ($errors->has('name'))
                <div class="invalid-feedback">{{ $errors->first('name') }}</div>
            @endif
        </div>
        <div class="mb-4">
            <label class="mb-1 text-dark">Email</label>
            <input type="email" class="form-control" placeholder="Email" id="email" name="email" value="{{ old('email') }}" required >
            @if ($errors->has('email'))
                <div class="invalid-feedback">{{ $errors->first('email') }}</div>
            @endif
        </div>
        <div class="mb-4 position-relative">
            <label class="mb-1 text-dark">Password</label>
            <input type="password"  class="form-control " placeholder="Password" id="password" name="password" value="{{ old('password') }}" required>
            <span class="show-pass eye">
            
                <i class="fa fa-eye-slash"></i>
                <i class="fa fa-eye"></i>
            
            </span>
              @if ($errors->has('password'))
                <div class="invalid-feedback">{{ $errors->first('password') }}</div>
            @endif
        </div>
        <div class="mb-4 position-relative">
            <label class="mb-1 text-dark">Confirm Password</label>
            <input type="password"  class="form-control " id="password_confirmation" name="password_confirmation" placeholder="Confirm Password" required>
            <span class="show-pass eye">
            
                <i class="fa fa-eye-slash"></i>
                <i class="fa fa-eye"></i>
            
            </span>
        </div>
        <div class="text-center mb-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
        </div>
        
        <p class="text-center">Registered?  
            <a class="btn-link text-primary" href="#">Login</a>
        </p>
    </form>
</div>
@endsection
@push('scripts')
<script>
    (function () {
		  'use strict'

		  // Fetch all the forms we want to apply custom Bootstrap validation styles to
		  var forms = document.querySelectorAll('.needs-validation')

		  // Loop over them and prevent submission
		  Array.prototype.slice.call(forms)
			.forEach(function (form) {
			  form.addEventListener('submit', function (event) {
				if (!form.checkValidity()) {
				  event.preventDefault()
				  event.stopPropagation()
				}

				form.classList.add('was-validated')
			  }, false)
			})
		})()

</script>
@endpush