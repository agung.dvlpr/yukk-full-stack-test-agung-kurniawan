<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Wallet;
use App\Models\WalletDetail;
use App\Models\Ms_Type;
use Validator;
use DB;
use Illuminate\Support\Str;

class WalletController extends Controller
{

    public function __construct()
    {
        $this->rules =  [
            'amount' => 'required',
            'typeid'  => 'required',
            'desc'    => 'required'
        ];
    }

    public function index(Request $request)
    {
        $search = $request->input('search');
        $type = $request->input('type');
        $daterange = $request->input('daterange');
        $data_type = Ms_Type::all();
        $data = WalletDetail::when($search, function ($q) use($search){
                    $q->where('debit', 'like', '%' . $search . '%')
                    ->orWhere('credit', 'like', '%' . $search . '%')
                    ->orWhere('desc', 'like', '%' . $search . '%')
                    ->orWhere('balance', 'like', '%' . $search . '%');                    
                })
                ->when($daterange, function ($q) use($daterange){
                    $datee = explode(' - ',$daterange);
                    $q->whereBetween('created_at', [$datee[0].' 00:00:00', $datee[1].' 23:59:59']);
                })
                ->when($type && $type != 0, function ($q) use($type){
                    $q->where('typeid', $type);
                })
                ->with(['type'])
                ->orderBy('created_at','desc')
                ->paginate(10);

        $response = [
            'data'=>$data,
            'search'=>$search,
            'type'=>$type,
            'data_type'=>$data_type,
            'daterange'=>$daterange
        ];
        return view('main.transaction')->with($response);
    }

    public function add(Request $request)
    {
        $data = Ms_Type::all();
        $response = [
            'title'=>'Topup / Transaksi',
            'type'=>$data,
            'data'=>null
        ];
        return view('main.add_or_edit')->with($response);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),$this->rules);
        if ($validator->fails()) {
            return back()->withErrors(['message' => $validator->errors()]);
        }

        DB::beginTransaction();
        try{
            $wallet = Wallet::where('userid', Auth::user()->id)->first();
            $balance = ($wallet) ? $wallet->balance : 0;
            $credit = 0;
            $debit = 0;
            if ($request->typeid == 1) {
                $balance = floatval($balance) + floatval($request->amount);
                $debit   = $request->amount;
            } else  {
                $balance = floatval($balance) - floatval($request->amount);
                $credit  = $request->amount;
            }

            if($balance < 0) {
                return back()->withErrors(['message'=>'Saldo anda kurang, silakan topup dulu!']);
            }

            if ($wallet) { 
                $walletDetail = WalletDetail::create([
                    'walletid'  => $wallet->walletid,
                    'walletcode'=> Str::random(8),
                    'typeid'    => $request->typeid,
                    'debit'     => $debit,
                    'credit'    => $credit,
                    'balance'   => $balance,
                    'desc'      => $request->desc,
                    'created_by'=> Auth::user()->id
                ]);

                $wallet = Wallet::where('walletid', $wallet->walletid)->update([
                    'balance'   => $balance,
                    'updated_by'=> Auth::user()->id
                ]);
            } else {
                $wallet = Wallet::create([
                    'userid'    => Auth::user()->id,
                    'balance'   => floatval($request->amount),
                    'created_by'=> Auth::user()->id
                ])->walletid;

                $walletDetail = WalletDetail::create([
                    'walletid'  => $wallet,
                    'walletcode'=> Str::random(8),
                    'typeid'    => $request->typeid,
                    'debit'     => $debit,
                    'credit'    => $credit,
                    'balance'   => $balance,
                    'desc'      => $request->desc,
                    'created_by'=> Auth::user()->id
                ]);
            }
            DB::commit();
            return redirect()->route('transaction')->withSuccess('Your transaction success!');
        } catch(\Exception $e){dd($e);
            DB::rollback();
            return back()->withErrors(['message'=>'Your transaction failed!']);
        }
    }
}
