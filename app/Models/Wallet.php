<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Wallet extends Model
{
    use HasFactory;

    public $timestamps=true;
    protected $table = 'wallet';
    protected $primaryKey = 'walletid';
    protected $fillable = ['balance', 'userid', 'created_by', 'updated_by'];
    protected $guarded = ['walletid','created_at','updated_at'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = isset(Auth::user()->id) ? Auth::user()->id:null;
            $model->updated_by = NULL;
        });

        static::updating(function ($model) {
            $model->updated_by = Auth::user()->id;
        });
    }

    
}
