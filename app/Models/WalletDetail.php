<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class WalletDetail extends Model
{
    use HasFactory;

    public $timestamps=true;
    protected $table = 'wallet_detail';
    protected $primaryKey = 'walletdetailid';
    protected $guarded = ['walletdetailid','created_at','updated_at'];
    protected $fillable = ['walletcode', 'walletid','typeid','debit','credit', 'balance','desc','created_by','updated_by'];

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            $model->created_by = isset(Auth::user()->id) ? Auth::user()->id:null;
            $model->updated_by = NULL;
        });

        static::updating(function ($model) {
            $model->updated_by = Auth::user()->id;
        });
    }

    public function type(){
		return $this->belongsTo('App\Models\Ms_Type','typeid');
	}
}
