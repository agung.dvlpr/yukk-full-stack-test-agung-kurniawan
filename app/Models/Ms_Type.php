<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ms_Type extends Model
{
    use HasFactory;
    public $timestamps=true;
    protected $table = 'ms_type';
    protected $primaryKey = 'typeid';
    protected $guarded = ['typeid','created_at','updated_at'];

    protected $fillable = [
        'type',
        'created_by',
        'updated_by',
    ];
}
