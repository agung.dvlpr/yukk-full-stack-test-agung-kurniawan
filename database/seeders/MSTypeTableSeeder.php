<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MSTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Using Eloquent model to create a user
        \App\Models\Ms_Type::create([
            'type' => 'topup',
        ]);

        // Using DB facade to insert data into the users table
        DB::table('ms_type')->insert([
            'type' => 'transaksi'
        ]);
    }
}
