<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('wallet_detail', function (Blueprint $table) {
            $table->increments('walletdetailid');
            $table->integer('walletid');
            $table->integer('typeid');
            $table->string('walletcode');
            $table->float('debit', 8, 2)->nullable();
            $table->float('credit', 8, 2)->nullable();
            $table->text('desc')->nullable();
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('wallet_detail');
    }
};
